#!/bin/sh

#+ Source [[https://gitlab.com/toms-dotfiles/utilities/-/blob/master/safe_linker.sh][safe_linker]]
safe_linker_url='https://gitlab.com/toms-dotfiles/utilities/-/raw/master/safe_linker.sh'
#-NOTE: POSIX compliant version of 'source <(curl -s "${safe_linker_url}")'
tmppipe=$(mktemp -u)
mkfifo -m 600 "${tmppipe}"
curl -s "${safe_linker_url}" >"${tmppipe}" &
. "${tmppipe}"

#+ Link the dots
safe_linker "$(pwd)"/starship.toml "${HOME}"/.config/starship.toml
